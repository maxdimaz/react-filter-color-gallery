import React, { Component } from "react";

import "./ColorGalery.scss";

interface ColorProps {
  color: string;
  category: string;
  darker: boolean;
}
export class ColorGalery extends Component<ColorProps> {
  render() {
    const style = {
      backgroundColor: this.props.color
    };
    return <div style={style} className="item-gallery"></div>;
  }
}

export default ColorGalery;
