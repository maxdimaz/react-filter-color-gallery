import React from "react";
import "./App.scss";

// component to create gallery
import Gallery from "./components/ColorGalery";

// interface of list color
interface Color {
  color: string;
  category: string;
  darker: boolean;
}

// All Color Data
const colorData: Array<Color> = [
  {
    color: "#E93F33",
    category: "red",
    darker: false
  },
  {
    color: "#2196F3",
    category: "blue",
    darker: false
  },
  {
    color: "#1b77c2",
    category: "blue",
    darker: true
  },
  {
    color: "#4CAF50",
    category: "green",
    darker: false
  },
  {
    color: "#009688",
    category: "green",
    darker: true
  },
  {
    color: "#FFEB3B",
    category: "yellow",
    darker: false
  },
  {
    color: "#FFC107",
    category: "yellow",
    darker: true
  }
];

// Count Gallery
const countGallery: number = 40;

class App extends React.Component {
  // State for List Gallery, filter categori and filter darker
  state = {
    gallery: [],
    category: "",
    darker: false
  };

  // Function to craete random color from colorData
  getRandomColor() {
    let gallery: Array<Color> = [];
    for (let i = 1; i <= countGallery; i++) {
      let colorIndex = Math.floor(Math.random() * colorData.length);
      let data = colorData[colorIndex];
      gallery = [...gallery, data];
    }

    // after create random color set to State
    this.setState({
      gallery: gallery
    });
  }

  // On Event will mount create random collor
  componentWillMount() {
    this.getRandomColor();
  }

  // handleChange select element
  handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const { value, name } = e.target;

    this.setState({
      [name]: value
    });
  };

  // handleChecked checkbox element
  handleChecked = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { checked, name } = e.target;

    this.setState({
      [name]: checked
    });
  };

  // render function
  render() {
    // destrukturing from state
    const { category, darker, gallery } = this.state;

    // create variable to store random color
    let listgallery: Array<Color> = gallery;

    // filter data by category
    if (category !== "") {
      listgallery = listgallery.filter(function(data) {
        return data.category === category;
      });
    }

    //filter data by darker
    if (darker !== false) {
      listgallery = listgallery.filter(function(data) {
        return data.darker === true;
      });
    }

    // List result gallery
    const itemGallery = listgallery.map(b => (
      <Gallery color={b.color} category={b.category} darker={b.darker} />
    ));

    // for option value dropdown category
    const options = [
      { value: "", label: "All" },
      { value: "red", label: "Red" },
      { value: "blue", label: "Blue" },
      { value: "green", label: "Green" },
      { value: "yellow", label: "Yellow" }
    ];

    return (
      <div className="wrap-page">
        <div className="form-area">
          <div className="input-wrap">
            <label htmlFor="category">Category</label>
            <select
              name="category"
              className="select-input"
              id="category"
              onChange={this.handleChange}
            >
              {options.map(option => (
                <option key={option.label} value={option.value.toString()}>
                  {option.label}
                </option>
              ))}
            </select>
          </div>
          <div className="input-wrap">
            <input
              type="checkbox"
              name="darker"
              value="true"
              id="darker"
              onChange={this.handleChecked}
            />

            <label htmlFor="darker">Show Darker Color</label>
          </div>
        </div>

        <div className="color-wrapper">{itemGallery}</div>
      </div>
    );
  }
}
export default App;
